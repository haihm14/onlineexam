namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EXAM")]
    public partial class EXAM
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string CODE { get; set; }

        [StringLength(50)]
        public string NAME { get; set; }

        public int? TOTAL_TIME { get; set; }

        public int? SCHEDULE_ID { get; set; }

        public int? SUBJECT_ID { get; set; }

        public short? STATUS { get; set; }
    }
}
