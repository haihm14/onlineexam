namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EXAM_QUIZ
    {
        public int ID { get; set; }

        public int? EXAM_ID { get; set; }

        public int? QUIZ_ID { get; set; }
    }
}
