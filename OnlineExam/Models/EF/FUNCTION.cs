namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FUNCTION")]
    public partial class FUNCTION
    {
        public int ID { get; set; }

        public int? PARENT_ID { get; set; }

        [StringLength(50)]
        public string NAME { get; set; }

        [StringLength(50)]
        public string URL { get; set; }

        [StringLength(50)]
        public string CREATED_BY { get; set; }

        public short? IS_SHOW { get; set; }

        public short? STATUS { get; set; }
    }
}
