namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QUIZ")]
    public partial class QUIZ
    {
        public int ID { get; set; }

        [Required]
        public string CONTENT { get; set; }

        public short? RESULT { get; set; }

        [StringLength(50)]
        public string AREA_CODE { get; set; }

        public byte? POINT { get; set; }

        public int? PARENT_ID { get; set; }

        public byte? TYPE { get; set; }

        public string MEDIA { get; set; }

        public DateTime? CREATED_DATE { get; set; }

        [StringLength(50)]
        public string CREATED_BY { get; set; }

        public DateTime? UPDATED_DATE { get; set; }

        [StringLength(50)]
        public string UPDATED_BY { get; set; }

        public byte? STATUS { get; set; }
    }
}
