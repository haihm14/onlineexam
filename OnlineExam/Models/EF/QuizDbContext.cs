namespace Models.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class QuizDbContext : DbContext
    {
        public QuizDbContext()
            : base("name=QuizDbContext")
        {
        }

        public virtual DbSet<EXAM> EXAMs { get; set; }
        public virtual DbSet<EXAM_QUIZ> EXAM_QUIZ { get; set; }
        public virtual DbSet<FUNCTION> FUNCTIONs { get; set; }
        public virtual DbSet<PARAMETPER> PARAMETPERs { get; set; }
        public virtual DbSet<QUIZ> QUIZs { get; set; }
        public virtual DbSet<ROLE> ROLEs { get; set; }
        public virtual DbSet<ROLE_FUNCTION> ROLE_FUNCTION { get; set; }
        public virtual DbSet<ROOM> ROOMs { get; set; }
        public virtual DbSet<SCHEDULE> SCHEDULEs { get; set; }
        public virtual DbSet<SCHEDULE_DETAIL> SCHEDULE_DETAIL { get; set; }
        public virtual DbSet<SUBJECT> SUBJECTs { get; set; }
        public virtual DbSet<SUBJECT_ROOM> SUBJECT_ROOM { get; set; }
        public virtual DbSet<USER> USERs { get; set; }
        public virtual DbSet<USER_ROLE> USER_ROLE { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
