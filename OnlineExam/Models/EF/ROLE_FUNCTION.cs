namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ROLE_FUNCTION
    {
        public int ID { get; set; }

        public int? ROLE_ID { get; set; }

        public int? FUNCTION_ID { get; set; }

        public DateTime? CREATED_DATE { get; set; }

        [StringLength(50)]
        public string CREATED_BY { get; set; }

        public DateTime? UPDATED_DATE { get; set; }

        [StringLength(50)]
        public string UPDATED_BY { get; set; }

        public short? STATUS { get; set; }
    }
}
