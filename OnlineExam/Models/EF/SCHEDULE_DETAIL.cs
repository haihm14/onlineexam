namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCHEDULE_DETAIL
    {
        public int ID { get; set; }

        public int? SUBJECT_ID { get; set; }

        public int? USER_ID { get; set; }

        public int? ROOM_ID { get; set; }

        public int? EXAM_ID { get; set; }

        public int? SCHEDULE_ID { get; set; }

        public DateTime? EXAM_DATE { get; set; }

        public short? STATUS { get; set; }
    }
}
