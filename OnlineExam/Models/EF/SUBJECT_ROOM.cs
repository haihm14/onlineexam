namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SUBJECT_ROOM
    {
        public int ID { get; set; }

        public int? SUBJECT_ID { get; set; }

        public int? ROOM_ID { get; set; }

        public DateTime? START_TIME { get; set; }

        public string LIST_PROCTOR_ID { get; set; }

        public string LIST_PROCTOR_NAME { get; set; }

        public short? PROCTOR_NUMBER { get; set; }

        public short? STATUS { get; set; }
    }
}
