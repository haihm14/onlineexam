namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("USER")]
    public partial class USER
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string USERNAME { get; set; }

        [StringLength(50)]
        public string PASSWORD { get; set; }

        public byte? TYPE { get; set; }

        [StringLength(50)]
        public string FULL_NAME { get; set; }

        [StringLength(50)]
        public string AVATAR { get; set; }

        [StringLength(50)]
        public string EMAIL { get; set; }

        [StringLength(15)]
        public string PHONE_NUMBER { get; set; }

        public DateTime? BIRTHDAY { get; set; }

        [StringLength(50)]
        public string UNIT { get; set; }

        [StringLength(50)]
        public string ADDRESS { get; set; }

        [StringLength(50)]
        public string TECHNIQUE { get; set; }

        public DateTime? CREATED_DATE { get; set; }

        [StringLength(50)]
        public string CREATED_BY { get; set; }

        public DateTime? UPDATED_DATE { get; set; }

        [StringLength(50)]
        public string UPDATED_BY { get; set; }

        public byte? STATUS { get; set; }
    }
}
