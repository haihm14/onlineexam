﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Manages.BaseInterface
{
    public abstract class BaseManage<T>
    {
        private QuizDbContext DBContext = null;
        protected QuizDbContext DB
        {
            get
            {
                if(DBContext ==  null)
                {
                    DBContext = new QuizDbContext();
                }
                return DBContext;
            }
        }

        public abstract List<T> GetAll();
        public abstract T GetById(int id);
        public abstract List<T> Search(T item);
        public abstract int Insert(T item);
        public abstract int Update(T item);
        public abstract int Delete(int id);
        public abstract int DeleteBatch(string listId);
        public abstract int InsertBatch(List<T> listItem);
        public abstract int UpdateBatch(List<T> listItem);



    }
}
