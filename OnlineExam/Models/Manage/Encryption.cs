﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Models.Manage
{
    public static class Encryption
    {
        public static string ToMD5(string str,string salt)
        {
            using (MD5 provider = MD5.Create())
            {
                byte[] bytes = provider.ComputeHash(Encoding.ASCII.GetBytes(salt + str));
                return BitConverter.ToString(bytes);
            }
                
        }
    }
}
