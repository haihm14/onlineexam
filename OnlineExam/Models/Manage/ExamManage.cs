﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class ExamManage : BaseManage<EXAM>
    {
        public override int Delete(int id)
        {
            var item =  DB.EXAMs.Find(id);
            item.STATUS = 0;
            return Update(item);
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<EXAM> GetAll()
        {
            return DB.EXAMs.ToList();
        }

        public override EXAM GetById(int id)
        {
            return DB.EXAMs.Find(id);
        }

        public override int Insert(EXAM item)
        {
            int idAdd =DB.EXAMs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<EXAM> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<EXAM> Search(EXAM item)
        {
            return DB.EXAMs.Where(x => x.CODE.ToLower().Contains(item.CODE.ToLower()) || x.SCHEDULE_ID == item.SCHEDULE_ID || x.STATUS == item.STATUS ||
            x.SUBJECT_ID == item.SUBJECT_ID || x.TOTAL_TIME == item.TOTAL_TIME || x.NAME.ToLower().Contains(item.NAME.ToLower()) || x.CODE.ToLower().Contains(item.CODE.ToLower())).ToList<EXAM>();// DB.EXAMs.Find(item);
        }

        public override int Update(EXAM item)
        {
            var oldItem = DB.EXAMs.Find(item.ID);
            if(oldItem != null){
                oldItem.CODE = item.CODE;
                oldItem.NAME = item.NAME;
                oldItem.SCHEDULE_ID = item.SCHEDULE_ID;
                oldItem.STATUS = item.STATUS;
                oldItem.SUBJECT_ID = item.SUBJECT_ID;
                oldItem.TOTAL_TIME = item.TOTAL_TIME;
                DB.SaveChanges();
            }
            return oldItem.ID;//DB.EXAMs.u
        }

        public override int UpdateBatch(List<EXAM> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
