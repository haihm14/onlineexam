﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class ExamQuizManage : BaseManage<EXAM_QUIZ>
    {
        public override int Delete(int id)
        {
            var item =  DB.EXAM_QUIZ.Find(id);
            return Update(item);
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<EXAM_QUIZ> GetAll()
        {
            return DB.EXAM_QUIZ.ToList();
        }

        public override EXAM_QUIZ GetById(int id)
        {
            return DB.EXAM_QUIZ.Find(id);
        }

        public override int Insert(EXAM_QUIZ item)
        {
            int idAdd =DB.EXAM_QUIZ.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<EXAM_QUIZ> listItem)
        {
            foreach (var item in listItem)
            {
                Insert(item);
            }
            return 1;
        }

        public override List<EXAM_QUIZ> Search(EXAM_QUIZ item)
        {
            return DB.EXAM_QUIZ.Where(x => x.EXAM_ID == item.EXAM_ID || x.QUIZ_ID == item.QUIZ_ID ).ToList<EXAM_QUIZ>();// DB.EXAMs.Find(item);
        }

        public override int Update(EXAM_QUIZ item)
        {
            var oldItem = DB.EXAM_QUIZ.Find(item.ID);
            if(oldItem != null){
                oldItem.EXAM_ID = item.EXAM_ID;
                oldItem.QUIZ_ID = item.QUIZ_ID;
               
                DB.SaveChanges();
            }
            return oldItem.ID;//DB.EXAMs.u
        }

        public override int UpdateBatch(List<EXAM_QUIZ> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
