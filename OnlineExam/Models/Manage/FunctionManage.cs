﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class FunctionManage : BaseManage<FUNCTION>
    {
        public override int Delete(int id)
        {
            var oldItem = DB.FUNCTIONs.Find(id);
            if (oldItem != null)
            {
                oldItem.STATUS = 0;
                DB.SaveChanges();
                return oldItem.ID;
            }
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<FUNCTION> GetAll()
        {
            return DB.FUNCTIONs.ToList();
        }

        public override FUNCTION GetById(int id)
        {
            return DB.FUNCTIONs.Find(id);
        }

        public override int Insert(FUNCTION item)
        {
            int idAdd = DB.FUNCTIONs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<FUNCTION> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<FUNCTION> Search(FUNCTION item)
        {
            return DB.FUNCTIONs.Where(x => x.CREATED_BY.ToLower().Contains(item.CREATED_BY.ToLower()) || x.STATUS == item.STATUS ||
            x.URL == item.URL || x.PARENT_ID == item.PARENT_ID ||
            x.NAME.ToLower().Contains(item.NAME.ToLower())).ToList<FUNCTION>();// DB.USERs.Find(item);
        }

        public override int Update(FUNCTION item)
        {
            var oldItem = DB.FUNCTIONs.Find(item.ID);
            if(oldItem != null)
            {
                oldItem.NAME = item.NAME;
                oldItem.STATUS = item.STATUS;
                oldItem.URL = item.URL;
                oldItem.PARENT_ID = item.PARENT_ID;
                oldItem.CREATED_BY = item.CREATED_BY;
                DB.SaveChanges();
                return oldItem.ID;
            }
            return 0;
        }

        public override int UpdateBatch(List<FUNCTION> listItem)
        {
            throw new NotImplementedException();
        }
         public List<FUNCTION> GetFunctionsByRoleId(int roleId)
        {
            List<FUNCTION> list =new List<FUNCTION>();
            List<ROLE_FUNCTION> listRoleFunction = null;
            listRoleFunction = DB.ROLE_FUNCTION.Where(x => x.ROLE_ID == roleId).ToList<ROLE_FUNCTION>();
            foreach(var item in listRoleFunction)
            {
                list.Add(DB.FUNCTIONs.Find(item.FUNCTION_ID));
            }
            return list;
        }
    }
}
