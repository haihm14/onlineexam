﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class ParametperManage : BaseManage<PARAMETPER>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.PARAMETPERs.Remove(new PARAMETPER { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<PARAMETPER> GetAll()
        {
            return DB.PARAMETPERs.ToList();
        }

        public override PARAMETPER GetById(int id)
        {
            return DB.PARAMETPERs.Find(id);
        }

        public override int Insert(PARAMETPER item)
        {
            int idAdd = DB.PARAMETPERs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<PARAMETPER> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<PARAMETPER> Search(PARAMETPER item)
        {
            return DB.PARAMETPERs.Where(x => x.NAME.ToLower().Contains(item.NAME.ToLower()) ||
            x.DESCRIPTION.ToLower().Contains(item.DESCRIPTION.ToLower()) ||x.STATUS == item.STATUS || x.TYPE == item.TYPE ||
            x.VALUE == item.VALUE ||
            x.CODE.ToLower().Contains(item.CODE.ToLower())).ToList<PARAMETPER>();// DB.USERs.Find(item);
        }

        public override int Update(PARAMETPER item)
        {
            var oldPara = DB.PARAMETPERs.Find(item.ID);
            if (oldPara != null)
            {
                oldPara.NAME = item.NAME;
                oldPara.STATUS = item.STATUS;
                oldPara.TYPE = item.TYPE;
                oldPara.VALUE = item.VALUE;
                oldPara.CODE = item.CODE;
                oldPara.DESCRIPTION = item.DESCRIPTION;
                DB.SaveChanges();
                return oldPara.ID;
            }
            return 0;//DB.USERs.u
        }

        public override int UpdateBatch(List<PARAMETPER> listItem)
        {
            throw new NotImplementedException();
        }
        public PARAMETPER getByCode(string code)
        {
            var list = DB.PARAMETPERs.Where(x => x.CODE.Contains(code)).ToList<PARAMETPER>();
            if (list.Count > 0)
            {
                return list.ElementAt(0);
            }
            else
            {
                return null;
            }


        }
    }
}
