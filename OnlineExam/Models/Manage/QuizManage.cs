﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class QuizManage : BaseManage<QUIZ>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.QUIZs.Remove(new QUIZ { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<QUIZ> GetAll()
        {
            return DB.QUIZs.ToList();
        }

        public override QUIZ GetById(int id)
        {
            return DB.QUIZs.Find(id);
        }

        public override int Insert(QUIZ item)
        {
            int idAdd = DB.QUIZs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<QUIZ> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<QUIZ> Search(QUIZ item)
        {
            return DB.QUIZs.Where(x => x.AREA_CODE.ToLower().Contains(item.AREA_CODE.ToLower()) ||x.CREATED_BY.ToLower().Contains(item.CREATED_BY.ToLower()) ||
            x.CREATED_DATE.Equals(item.CREATED_DATE) || x.MEDIA == item.MEDIA || x.PARENT_ID == item.PARENT_ID || x.POINT == item.POINT || x.RESULT == item.RESULT || x.STATUS == item.STATUS||
            x.TYPE == item.TYPE ||
            x.CONTENT.ToLower().Contains(item.CONTENT.ToLower()) || 
            x.AREA_CODE.ToLower().Contains(item.AREA_CODE.ToLower())).ToList<QUIZ>();
        }

        public override int Update(QUIZ item)
        {
            var oldQuiz = DB.QUIZs.Find(item.ID);
            if(oldQuiz != null)
            {
                oldQuiz.MEDIA = item.MEDIA;
                oldQuiz.PARENT_ID = item.PARENT_ID;
                oldQuiz.POINT = item.POINT;
                oldQuiz.RESULT = item.RESULT;
                oldQuiz.STATUS = item.STATUS;
                oldQuiz.TYPE = item.TYPE;
                oldQuiz.UPDATED_DATE = DateTime.Now;
                oldQuiz.AREA_CODE = item.AREA_CODE;
                oldQuiz.CONTENT = item.CONTENT;

                DB.SaveChanges();
                return oldQuiz.ID;
            }
            return 0;//DB.USERs.u
        }

        public override int UpdateBatch(List<QUIZ> listItem)
        {
            throw new NotImplementedException();
        }
        public List<QUIZ> getListByExam(EXAM exam)
        {
            List<QUIZ> list = new List<QUIZ>();
            ScheduleDetailManage scheduleDeMa = new ScheduleDetailManage();
            var listShe = DB.SCHEDULE_DETAIL.Where(item => item.EXAM_ID == exam.ID);
            foreach(var item in listShe)
            {
                list.Add(GetById(item.ID));
            }
            return list;

        }
    }
}
