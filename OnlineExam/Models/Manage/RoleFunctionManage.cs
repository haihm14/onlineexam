﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class RoleFunctionManage : BaseManage<ROLE_FUNCTION>
    {
        public override int Delete(int id)
        {
            var idRemove = DB.ROLE_FUNCTION.Remove(new ROLE_FUNCTION { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;

            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<ROLE_FUNCTION> GetAll()
        {
            return DB.ROLE_FUNCTION.ToList();
        }

        public override ROLE_FUNCTION GetById(int id)
        {
            return DB.ROLE_FUNCTION.Find(id);
        }

        public override int Insert(ROLE_FUNCTION item)
        {
            int idAdd = DB.ROLE_FUNCTION.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<ROLE_FUNCTION> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<ROLE_FUNCTION> Search(ROLE_FUNCTION item)
        {
            return DB.ROLE_FUNCTION.Where(x => x.ROLE_ID == item.ROLE_ID || x.FUNCTION_ID == item.FUNCTION_ID || x.STATUS == item.STATUS || x.CREATED_BY.ToLower().Contains(item.CREATED_BY.ToLower())).ToList<ROLE_FUNCTION>();
        }

        public override int Update(ROLE_FUNCTION item)
        {
            var oldRF = DB.ROLE_FUNCTION.Find(item.ID);
            if (oldRF != null)
            {
                oldRF.ROLE_ID = item.ROLE_ID;
                oldRF.STATUS = item.STATUS;
                oldRF.UPDATED_BY = item.UPDATED_BY;
                oldRF.UPDATED_DATE = DateTime.Now;
                oldRF.CREATED_DATE = item.CREATED_DATE;
                oldRF.FUNCTION_ID = item.FUNCTION_ID;

                DB.SaveChanges();
                return oldRF.ID;
            }
            return 0;//DB.ROLE_FUNCTIONs.u
        }

        public override int UpdateBatch(List<ROLE_FUNCTION> listItem)
        {
            throw new NotImplementedException();
        }
        public List<FUNCTION> getFunctionByRoleId(int id)
        {
            List<FUNCTION> list = new List<FUNCTION>();
            var l = DB.ROLE_FUNCTION.Where(x => x.ROLE_ID.Value == id).ToList<ROLE_FUNCTION>();
            foreach (var item in l)
            {
                list.Add(DB.FUNCTIONs.Find(item.FUNCTION_ID));
            }
            return list;
        }
    }
}
