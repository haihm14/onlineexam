﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class RoleManage : BaseManage<ROLE>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.ROLEs.Remove(new ROLE { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<ROLE> GetAll()
        {
            return DB.ROLEs.ToList();
        }

        public override ROLE GetById(int id)
        {
            return DB.ROLEs.Find(id);
        }

        public override int Insert(ROLE item)
        {
            var idAdd = DB.ROLEs.Add(item).ID;
            if(DB.SaveChanges() > 0)
            {
                return idAdd;
            }
            return 0;
        }

        public override int InsertBatch(List<ROLE> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<ROLE> Search(ROLE item)
        {
            return DB.ROLEs.Where(x => x.CREATED_BY.ToLower().Contains(item.CREATED_BY.ToLower())|| x.DESCRIPTION.Contains(item.DESCRIPTION) || x.STATUS == item.STATUS ||
            x.CODE.ToLower().Contains(item.CODE.ToLower()) || x.NAME.ToLower().Contains(item.NAME.ToLower())).ToList<ROLE>();
        }

        public override int Update(ROLE item)
        {
            var oldRole = DB.ROLEs.Find(item.ID);
            if(oldRole != null)
            {
                oldRole.NAME = item.NAME;
                oldRole.STATUS = item.STATUS;
                oldRole.CODE = item.CODE;
                oldRole.CREATED_BY = item.CREATED_BY;
                oldRole.CREATED_DATE = item.CREATED_DATE;
                oldRole.DESCRIPTION = item.DESCRIPTION;

                DB.SaveChanges();
                return oldRole.ID;
            }
            return 0;//DB.ROLEs.u
        }

        public override int UpdateBatch(List<ROLE> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
