﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class RoomManage : BaseManage<ROOM>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.ROOMs.Remove(new ROOM { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<ROOM> GetAll()
        {
            return DB.ROOMs.ToList();
        }

        public override ROOM GetById(int id)
        {
            return DB.ROOMs.Find(id);
        }

        public override int Insert(ROOM item)
        {
            int idAdd = DB.ROOMs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<ROOM> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<ROOM> Search(ROOM item)
        {
            return DB.ROOMs.Where(x => x.STATUS == item.STATUS ||
            x.ADDRESS.ToLower().Contains(item.ADDRESS.ToLower()) || x.NAME.ToLower().Contains(item.NAME.ToLower())).ToList<ROOM>();
        }

        public override int Update(ROOM item)
        {
            var oldRoom = DB.ROOMs.Find(item.ID);
            if (oldRoom != null)
            {
                oldRoom.NAME = item.NAME;
                oldRoom.STATUS = item.STATUS;
                oldRoom.ADDRESS = item.ADDRESS;
                DB.SaveChanges();
                return oldRoom.ID;
            }
            return 0;//DB.ROOMs.u
        }

        public override int UpdateBatch(List<ROOM> listItem)
        {
            throw new NotImplementedException();
        }

        public List<USER> getListUserInRoom(ROOM room)
        {
            List<USER> list = new List<USER>();
            //List<U>
            return list;
        }
    }
}
