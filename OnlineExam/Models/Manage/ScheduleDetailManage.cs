﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;
using System.Collections;

namespace Models.Manage
{
    public class ScheduleDetailManage : BaseManage<SCHEDULE_DETAIL>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.SCHEDULE_DETAIL.Remove(new SCHEDULE_DETAIL { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<SCHEDULE_DETAIL> GetAll()
        {
            return DB.SCHEDULE_DETAIL.ToList();
        }

        public override SCHEDULE_DETAIL GetById(int id)
        {
            return DB.SCHEDULE_DETAIL.Find(id);
        }

        public override int Insert(SCHEDULE_DETAIL item)
        {
            int idAdd = DB.SCHEDULE_DETAIL.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<SCHEDULE_DETAIL> listItem)
        {
            foreach(var item in listItem)
            {
                Insert(item); 
            }
            return 1;
        }

        public override List<SCHEDULE_DETAIL> Search(SCHEDULE_DETAIL item)
        {
            return DB.SCHEDULE_DETAIL.Where(x => x.EXAM_ID == item.EXAM_ID || x.ROOM_ID == item.ROOM_ID || x.SCHEDULE_ID == item.SCHEDULE_ID ||
            x.SUBJECT_ID == item.SUBJECT_ID || x.USER_ID == item.SUBJECT_ID || x.EXAM_DATE.Equals(item.EXAM_DATE) ||
            x.STATUS == item.STATUS).ToList<SCHEDULE_DETAIL>();
        }

        public override int Update(SCHEDULE_DETAIL item)
        {

            var oldRoom = DB.SCHEDULE_DETAIL.Find(item.ID);
            if (oldRoom != null)
            {
                oldRoom.STATUS = item.STATUS;
                oldRoom.ROOM_ID = item.ROOM_ID;
                oldRoom.SCHEDULE_ID = item.SCHEDULE_ID;
                oldRoom.SUBJECT_ID = item.SUBJECT_ID;
                oldRoom.USER_ID = item.USER_ID;
                oldRoom.EXAM_DATE = item.EXAM_DATE;
                oldRoom.EXAM_ID = item.EXAM_ID;


                DB.SaveChanges();
                return oldRoom.ID;
            }
            return 0;//DB.USERs.u
        }

        public override int UpdateBatch(List<SCHEDULE_DETAIL> listItem)
        {
            foreach(var item in listItem)
            {
                Update(item);
            }
            return 1;
        }
        //public Hashtable GetCombo(string table,string columID,string columNAME)
        // {
        //     var has = new Hashtable();

        //     if(table == "USER")
        //     {

        //     }
        //     return has;

        // }
        public Hashtable GetComboUser(object item)
        {
            var has = new Hashtable();
            var list = new UserManage().Search((USER)item);
            return has;

        }
    }
}
