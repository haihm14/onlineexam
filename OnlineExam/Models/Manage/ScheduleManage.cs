﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class ScheduleManage : BaseManage<SCHEDULE>
    {
        public override int Delete(int id)
        {
            var scheduleItem = DB.SCHEDULEs.Find(id);
            scheduleItem.STATUS = 0;
            return Update(scheduleItem);
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<SCHEDULE> GetAll()
        {
            return DB.SCHEDULEs.ToList();
        }

        public override SCHEDULE GetById(int id)
        {
            return DB.SCHEDULEs.Find(id);
        }

        public override int Insert(SCHEDULE item)
        {
            int idAdd = DB.SCHEDULEs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<SCHEDULE> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<SCHEDULE> Search(SCHEDULE item)
        {
            if (item.START_DATE != null && item.END_DATE != null)
                return DB.SCHEDULEs.Where(x => x.STATUS == item.STATUS && x.START_DATE >= item.START_DATE && x.END_DATE <= item.END_DATE).ToList<SCHEDULE>();
            return DB.SCHEDULEs.Where(x => x.CREATED_DATE.Equals(item.CREATED_DATE) || x.DESCRIPTION.Contains(item.DESCRIPTION) || x.STATUS == item.STATUS ||
            x.CODE.ToLower().Contains(item.CODE.ToLower()) || x.NAME.ToLower().Contains(item.NAME.ToLower())).ToList<SCHEDULE>();
        }

        public override int Update(SCHEDULE item)
        {
            var old = DB.SCHEDULEs.Find(item.ID);
            if(old != null)
            {
                old.NAME = item.NAME;
                old.START_DATE = item.START_DATE;
                old.STATUS = item.STATUS;
                old.CODE = item.CODE;
                old.CREATED_DATE = item.CREATED_DATE;
                old.DESCRIPTION = item.DESCRIPTION;
                old.END_DATE = item.END_DATE;

                DB.SaveChanges();
                return old.ID;
            }
            return 0;//DB.USERs.u
        }

        public override int UpdateBatch(List<SCHEDULE> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
