﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class SubjectManage : BaseManage<SUBJECT>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.SUBJECTs.Remove(new SUBJECT { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<SUBJECT> GetAll()
        {
            return DB.SUBJECTs.ToList();
        }

        public override SUBJECT GetById(int id)
        {
            return DB.SUBJECTs.Find(id);
        }

        public override int Insert(SUBJECT item)
        {
            int idAdd = DB.SUBJECTs.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<SUBJECT> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<SUBJECT> Search(SUBJECT item)
        {
            return DB.SUBJECTs.Where(x => x.CREATED_BY.Contains(item.CREATED_BY) || x.CREDIT == item.CREDIT || x.TEACHER.Contains(item.TEACHER) || x.TEACHER_ID == item.ID || x.STATUS == item.STATUS||
            x.CREATED_BY.Contains(item.CREATED_BY)||
            x.CODE.ToLower().Contains(item.CODE.ToLower()) || x.NAME.ToLower().Contains(item.NAME.ToLower())).ToList<SUBJECT>();
        }

        public override int Update(SUBJECT item)
        {
            var old = DB.SUBJECTs.Find(item.ID);
            if (old != null)
            {
                old.NAME = item.NAME;
                old.CREATED_BY = item.CREATED_BY;
                old.STATUS = item.STATUS;
                old.CODE = item.CODE;
                old.CREATED_DATE = item.CREATED_DATE;
                old.CREDIT = item.CREDIT;
                old.TEACHER = item.TEACHER;
                old.TEACHER_ID = item.TEACHER_ID;

                DB.SaveChanges();
                return old.ID;
            }
            return 0;//DB.SUBJECTs.u
        }

        public override int UpdateBatch(List<SUBJECT> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
