﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class SubjectRoomManage : BaseManage<SUBJECT_ROOM>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.SUBJECT_ROOM.Remove(new SUBJECT_ROOM { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<SUBJECT_ROOM> GetAll()
        {
            return DB.SUBJECT_ROOM.ToList();
        }

        public override SUBJECT_ROOM GetById(int id)
        {
            return DB.SUBJECT_ROOM.Find(id);
        }

        public override int Insert(SUBJECT_ROOM item)
        {
            int idAdd = DB.SUBJECT_ROOM.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<SUBJECT_ROOM> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<SUBJECT_ROOM> Search(SUBJECT_ROOM item)
        {
            return DB.SUBJECT_ROOM.Where(x => x.LIST_PROCTOR_ID == item.LIST_PROCTOR_ID || x.LIST_PROCTOR_NAME.Contains(item.LIST_PROCTOR_NAME) ||x.PROCTOR_NUMBER == item.PROCTOR_NUMBER || x.ROOM_ID == item.ROOM_ID ||
            x.SUBJECT_ID == item.SUBJECT_ID || x.START_TIME >= item.START_TIME ||
            x.LIST_PROCTOR_NAME.ToLower().Contains(item.LIST_PROCTOR_NAME.ToLower()) || x.STATUS== item.STATUS).ToList<SUBJECT_ROOM>();
        }

        public override int Update(SUBJECT_ROOM item)
        {
            var old = DB.SUBJECT_ROOM.Find(item.ID);
            if (old != null)
            {
                old.SUBJECT_ID = item.SUBJECT_ID;
                old.STATUS = item.STATUS;
                old.START_TIME = item.START_TIME;
                old.ROOM_ID = item.ROOM_ID;
                old.PROCTOR_NUMBER = item.PROCTOR_NUMBER;
                old.LIST_PROCTOR_NAME = item.LIST_PROCTOR_NAME;
                old.LIST_PROCTOR_ID = item.LIST_PROCTOR_ID;

                DB.SaveChanges();
                return old.ID;
            }
            return 0;//DB.SUBJECT_ROOM.u
        }

        public override int UpdateBatch(List<SUBJECT_ROOM> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
