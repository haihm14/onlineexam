﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class UserManage : BaseManage<USER>
    {
        public override int Delete(int id)
        {
            var oldItem = DB.USERs.Find(id);
            if (oldItem != null)
            {
                oldItem.STATUS = 0;
                DB.SaveChanges();
                return oldItem.ID;
            }
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<USER> GetAll()
        {
            return DB.USERs.ToList();
        }

        public override USER GetById(int id)
        {
            return DB.USERs.Find(id);
        }

        public override int Insert(USER item)
        {
            DB.USERs.Add(item);
            return DB.SaveChanges();
        }

        public override int InsertBatch(List<USER> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<USER> Search(USER item)
        {
            return DB.USERs.Where(x => x.USERNAME.ToLower().Contains(item.USERNAME.ToLower()) || 
            x.EMAIL.ToLower().Contains(item.EMAIL.ToLower()) ||
            x.FULL_NAME.ToLower().Contains(item.FULL_NAME.ToLower()) 
            ).Where(x => x.STATUS ==1).ToList<USER>();// DB.USERs.Find(item);
        }

        public int CheckLogin(string username,string password,string salt)
        {
            var ecrypt = Encryption.ToMD5(password, salt);
            var user = DB.USERs.Where(x => x.USERNAME.Contains(username) && x.PASSWORD.Contains(ecrypt)).First();
            if (user != null)
            {
                return user.ID;
            }
            return -1;
        }

        public override int Update(USER item)
        {
            var oldItem = DB.USERs.Find(item.ID);
            if (oldItem != null)
            {
                oldItem.FULL_NAME = item.FULL_NAME;
                oldItem.USERNAME = item.USERNAME;
                oldItem.TYPE = item.TYPE;
                oldItem.UNIT = item.UNIT;
                oldItem.UPDATED_BY = item.UPDATED_BY;
                oldItem.UPDATED_DATE = item.UPDATED_DATE;
                oldItem.TECHNIQUE = item.TECHNIQUE;
                oldItem.PHONE_NUMBER = item.PHONE_NUMBER;
                oldItem.EMAIL = item.EMAIL;
                oldItem.BIRTHDAY = item.BIRTHDAY;
                oldItem.AVATAR = item.AVATAR;
                oldItem.STATUS = item.STATUS;
                oldItem.ADDRESS = item.ADDRESS;
                DB.SaveChanges();
                return oldItem.ID;
            }
            return 0;
        }

        public override int UpdateBatch(List<USER> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
