﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using Models.Manages.BaseInterface;

namespace Models.Manage
{
    public class UserRoleManage : BaseManage<USER_ROLE>
    {
        public override int Delete(int id)
        {
            int idRemove = DB.USER_ROLE.Remove(new USER_ROLE { ID = id }).ID;
            if (DB.SaveChanges() > 0)
                return idRemove;
            return 0;
        }

        public override int DeleteBatch(string listId)
        {
            throw new NotImplementedException();
        }

        public override List<USER_ROLE> GetAll()
        {
            return DB.USER_ROLE.ToList();
        }

        public override USER_ROLE GetById(int id)
        {
            return DB.USER_ROLE.Find(id);
        }

        public override int Insert(USER_ROLE item)
        {
            int idAdd = DB.USER_ROLE.Add(item).ID;
            if (DB.SaveChanges() > 0)
                return idAdd;
            return 0;
        }

        public override int InsertBatch(List<USER_ROLE> listItem)
        {
            throw new NotImplementedException();
        }

        public override List<USER_ROLE> Search(USER_ROLE item)
        {
            return DB.USER_ROLE.Where(x => x.ROLE_ID == item.ROLE_ID || x.USER_ID == item.USER_ID || x.CREATED_BY.ToLower().Contains(item.CREATED_BY.ToLower()) || x.STATUS == item.STATUS).ToList<USER_ROLE>();
        }

        public override int Update(USER_ROLE item)
        {
            var oldRoom = DB.USER_ROLE.Find(item.ID);
            if (oldRoom != null)
            {
                oldRoom.ROLE_ID = item.ROLE_ID;
                oldRoom.STATUS = item.STATUS;
                oldRoom.UPDATED_BY = item.UPDATED_BY;
                oldRoom.UPDATED_DATE = item.UPDATED_DATE;
                oldRoom.CREATED_BY = item.CREATED_BY;
                oldRoom.CREATED_DATE = item.CREATED_DATE;

                DB.SaveChanges();
                return oldRoom.ID;
            }
            return 0;//DB.USERs.u
        }

        public override int UpdateBatch(List<USER_ROLE> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
