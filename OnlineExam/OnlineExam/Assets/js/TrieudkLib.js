﻿
var lib = function (name) {
    // ----------------------------------------      config       -------------------------------------------------------------
    this.defaultOption = {
        ClassInput: "input-quiz",
        URLService: {
            List: "/" + name + "/List",
            Detail:"/"+name+"/Detail",
            Search: "/" + name + "/Search",
            Edit: "/" + name + "/Update",
            Insert: "/" + name + "/Insert",
            Delete: "/" + name + "/Delete",
            MultipleDelete: "/" + name + "/MultipleDelete",
            MultipleInsert: "/" + name + "/MultipleInsert"
        },
        TemplateSelector: {
            EditView: "#" + name + "-editview",
            SearchView: "#" + name + "-searchview",
            ListView: "#" + name + "-listview",
        },
        ModalSelector: {
            Edit: "#editmodal",
            Add: "#editmodal",
            Detail: "#detailmodal",
        },
        TemplateFileName: {
            Edit: "/Views/" + name + "/edit.html",
            List: "/Views/" + name + "/list.html",
            Search: "/Views/" + name + "/search.html",
            Detail: "/Views/" + name + "/detail.html",
            Add: "/Views/" + name + "/edit.html",
        },
        //MustacheSelector: {
        //    EditView: "#" + name + "-editview-tmp",
        //    SearchView: "#" + name + "-searchview-tmp",
        //    ListView: "#" + name + "-listview-tmp",
        //},
        Notication: {
            AddSuccess: "Thêm mới thành công!",
            AddFaile: "Thêm mới không thành công!",
            EditSuccess: "Lưu lại thành công!",
            EditFail: "Lưu lại không thành công!",
            DefaultSucces: "Thành công!",
            DefaultFailt: "Không thành công!"
        }
    }
    var defaultOption = this.defaultOption;
    // ----------------------------------------      format data       -------------------------------------------------------------
    this.customeData = function customeData(data,isObject) {
        // chưa viết
        var result = [];
        if (isObject != undefined) {
            data = [data];
        }
        for (var item  in data) {
            data[item].NEW_STATUS = data[item].STATUS == 1 ? "Hiệu lực" : "Hết hiệu lực";


            result.push(data[item]);
        }
        return result;
    }
    // ----------------------------------------      init       -------------------------------------------------------------
    this.initView = function initView() {
        list();
        showSearchView();
    }
    // ----------------------------------------      show data       -------------------------------------------------------------
    this.showListview = function showListview(data) {
        MustacheBuilt(defaultOption.TemplateFileName.List, data,  defaultOption.TemplateSelector.ListView);
    }
    this.showSearchView = function showSearchView() {
        var data = {};
        MustacheBuilt(defaultOption.TemplateFileName.Search, data, defaultOption.TemplateSelector.SearchView);
    }
    //this.showEditview = function showEditview(data) {
    //    MustacheBuilt(defaultOption.TemplateFileName.Search, data, defaultOption.TemplateSelector.SearchView);
    //    $(defaultOption.ModalSelector.Edit).modal('show');
    //}
    this.showMessage = function showMessage(type, text) {

    }
    this.showDetailModal = function showDetailModal(id) {
        var data = detail(id);
        MustacheBuilt(defaultOption.TemplateFileName.Detail, data, defaultOption.ModalSelector.Detail + " .modal-body");
        $(defaultOption.ModalSelector.Detail).modal("show");
    }
    this.showEditModal = function showEditModal(id) {
        var data = detail(id);
        MustacheBuilt(defaultOption.TemplateFileName.Edit, data, defaultOption.ModalSelector.Edit + " .modal-body");
        $(defaultOption.ModalSelector.Edit).modal("show");
    }
    this.showAddModal = function showAddModal() {
        var data = { data: { trash: "1" } };
        MustacheBuilt(defaultOption.TemplateFileName.Add, data, defaultOption.ModalSelector.Add + " .modal-body");
        $(defaultOption.ModalSelector.Add).modal("show");
    }
    // ----------------------------------------      savedata       -------------------------------------------------------------
    this.save = function save(action) {
        if (action == "edit") {
            var item = JSON.parse(getJsonForm("form-edit"));
            mergItem = detail(item.ID);
            $.extend(true,mergItem,item);
            edit(JSON.stringify(mergItem));
        }
        if (action == "add") {
            var item = getJsonForm("form-edit");
            add(item);
        }
    }
    // ----------------------------------------      getdata       -------------------------------------------------------------
    //this.getObjectForm = function getObjectForm(selector) {
    //    var listInput = $("#" + formSelector + " input");
    //    var obj = "{";
    //    var i;
    //    for (i = 0; i < listInput.length; i++) {
    //        obj = obj + "\"" + $(listInput[i]).attr("md") + "\":";
    //        obj = obj + ($(listInput[i]).val() != null ? "\"" + $(listInput[i]).val() + "\"" : "null");
    //        if (i < listInput.length - 1) obj = obj + ",";
    //    }
    //    obj = obj + "}";
    //    return JSON.parse(obj);
    //}
    this.getJsonForm = function getJsonForm(formSelector) {
        var listInput = $("#" + formSelector + " input");
        var obj = "{";
        var i;
        for (i = 0; i < listInput.length; i++) {
            obj = obj + "\"" + $(listInput[i]).attr("md") + "\":";
            obj = obj + ($(listInput[i]).val() != null ? "\"" + $(listInput[i]).val() + "\"" : "null");
            if (i < listInput.length - 1) obj = obj + ",";
        }
        obj = obj + "}";
        return obj;
    }
    // ----------------------------------------      api       -------------------------------------------------------------
    this.add = function add(data) {
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.Insert,
            cache: false,
            dataType: "json",
            data: { jsonString: data },
            async: true,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.mes == "OK") {
                    list();
                    showMessage("Success", defaultOption.Notication.AddSuccess);
                } else {
                    showMessage("Fail", defaultOption.Notication.AddFaile);
                }
            },
            complete: function (data) {
                list();
            }
        });
    }
    this.edit = function edit(data) {
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.Edit,
            cache: false,
            dataType: "json",
            data: { jsonString: data },
            async: true,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.mes == "OK") {
                    list();
                    showMessage("Success", defaultOption.Notication.EditSuccess);
                } else {
                    showMessage("Fail", defaultOption.Notication.EditFail);
                }
            },
            complete: function () {
            }
        });
    }
    this.remove = function remove(id) {
        var data;
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.Delete,
            cache: false,
            dataType: "json",
            data: { id: id },
            async: true,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.mes == "OK") {
                    list();
                    showMessage("Success", defaultOption.Notication.DefaultSucces);
                } else {
                    showMessage("Fail", defaultOption.Notication.DefaultFailt);
                }
            },
            complete: function () {
                list();
            }
        });
    }
    this.search = function search() {
        var data;
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.Search,
            cache: false,
            dataType: "json",
            data: { jsonString: data },
            async: true,
            beforeSend: function () {
            },
            success: function (data) {
                showListview(customeData(data));
            },
            complete: function () {
            }
        });
    }
    this.list = function list() {
        var data;
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.List,
            cache: false,
            dataType: "json",
            data: data,
            async: false,
            beforeSend: function () {
            },
            success: function (list) {
                showListview(customeData(list));
            },
            complete: function (list) {
            }
        });
    }
    this.detail = function detail(id) {
        var data;
        $.ajax({
            type: "POST",
            url: defaultOption.URLService.Detail,
            cache: false,
            dataType: "json",
            data: { id: id },
            async: false,
            beforeSend: function () {
            },
            success: function (list) {
                data = customeData(list,"isObject");
            },
            complete: function () {
            }
        });
        return data;
    }
    // ----------------------------------------      mustache       -------------------------------------------------------------
    this.MustacheLoad = function MustacheLoad(file) {
        var ResultData =[];
        if ( file != "" && !!file) {
            $.ajax({
                type: "POST",
                url: "/Base/loadTemplate",
                cache: false,
                dataType: "html",
                data: { fileName: file },
                async: false,
                beforeSend: function () {
                },
                success: function (data) {
                    ResultData = data;
                },
                complete: function (data) {
                    //ResultData = data;
                }
            });
        }
        
        return ResultData;
    }
    this.MustacheBuilt = function MustacheBuilt(templateFile, data, mustacheIdDom) {
        data = data == "undefined" ? { data: "s" } : { data: data };
        var tmpl = MustacheLoad(templateFile);
        $(mustacheIdDom).empty();
        $(mustacheIdDom).html(Mustache.render(tmpl, data));
    }
    
    // ----------------------------------------      return       -------------------------------------------------------------
    return this;
}
