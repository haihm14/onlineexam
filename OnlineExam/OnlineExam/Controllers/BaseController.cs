﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models.Manages;
using Models;
using System.Collections;
using Models.Manage;

namespace OnlineExam.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var url = filterContext.RequestContext.HttpContext.Request.RawUrl;

            if (Session["CurentUser"] == null)
            {
                filterContext.Result = new RedirectResult(Url.Action("Login", "Login"));
            }
            else if (url != "/" && url != "/Base/loadTemplate/" && url != "/User/GetExamByStudentID" && url != "/Quiz/Search" && url != "/Quiz/addQuestion")
            {
                UserContext userContext =(UserContext)Session["UserContext"];
                if (userContext.HasPermistion(filterContext.RequestContext.HttpContext.Request.RawUrl))
                {
                    base.OnActionExecuted(filterContext);
                }
                else
                {
                    filterContext.Result = new RedirectResult(Url.Action("Login", "Login"));
                    return;
                }
                // check csrf if()
            }
        }
        //check
        [HttpPost]
        public string loadTemplate(string fileName)
        {
            string result = "";
            if (System.IO.File.Exists(Server.MapPath(fileName)))
            {
                result = System.IO.File.ReadAllText(Server.MapPath(fileName));
            }
            return result;
        }

    }
}