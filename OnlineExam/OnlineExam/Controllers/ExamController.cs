﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class ExamController : BaseController
    {
        // GET: Exam
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var exam = JsonConvert.DeserializeObject<EXAM>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            exam.STATUS = 1;
            int res = new ExamManage().Insert(exam);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            var exam = JsonConvert.DeserializeObject<EXAM>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            int res = new ExamManage().Update(exam);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new ExamManage().GetAll());
        }

        public string Detail(int id)
        {
            var a = new ExamManage().GetById(id);
            return JsonConvert.SerializeObject(new ExamManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new ExamManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}