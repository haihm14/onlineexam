﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class FunctionController : Controller
    {
        // GET: function
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var function = JsonConvert.DeserializeObject<FUNCTION>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            //function.CREATED_DATE = dt;
            //function.CREATED_BY = functionContext.functionName;
            function.STATUS = 1;
            int res = new FunctionManage().Insert(function);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var function = JsonConvert.DeserializeObject<FUNCTION>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //function function = (function)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            //function.UPDATED_DATE = dt;
            //function.UPDATED_BY = functionContext.functionName;
            int res = new FunctionManage().Update(function);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new FunctionManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new FunctionManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new FunctionManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}