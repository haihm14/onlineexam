﻿using Models;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineExam.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menu()
        {
            var UserContext = (UserContext)Session["UserContext"];

            if (UserContext != null)
            {
                ViewBag.List = UserContext.Permision;
            }
            else
            {
                ViewBag.List = new List<FUNCTION>();
            }
            return PartialView();
        }

        public ActionResult SideBar()
        {
            var user = (USER)Session["CurentUser"];
            if (user != null)
            {
                ViewBag.User = user;
            }
            else
            {
                ViewBag.User = null;
            }
            return PartialView();
        }


        public ActionResult StartContest()
        {

            return View();
        }

    }
}