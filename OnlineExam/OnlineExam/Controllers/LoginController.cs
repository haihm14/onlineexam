﻿
using Models;
using Models.EF;
using Models.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineExam.Controllers
{
    public class LoginController : Controller
    {
        private const string _loginPage = "~/Views/Login/Index.cshtml";
        private const string _homePage = "~/Views/Home/Index.cshtml";
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            string userName = Request.Form["txtUserName"];
            string password = Request.Form["txtPassword"];
            if (userName != null && userName != "" && password != null && password != "")
            {
                var pm = new ParametperManage().getByCode("SALT");
                string salt = pm == null ? "" : pm.VALUE;
                var userId = new UserManage().CheckLogin(userName, password, salt);

                if (userId > 0)
                {
                    createUserContext(userId);
                    return RedirectToAction("Index", "Parametper");
                }
                else
                {
                    FormsAuthentication.RedirectFromLoginPage(userName, false);
                }
            }
            return RedirectToAction("Index", "Login");

        }
        public ActionResult Logout()
        {
            Session["CurentUser"] = null;
            Session["UserContext"] = null;
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return RedirectToAction("Index", "Login");

        }
        private void createUserContext(int userId)
        {
            USER CurentUser = new UserManage().GetById(userId);
            List<FUNCTION> perList = new List<FUNCTION>();
            List<USER_ROLE> userRoleList = new UserRoleManage().Search(new USER_ROLE { USER_ID = userId });
            var rolefuMan = new RoleFunctionManage();
            RoleManage roleManage = new RoleManage();
            List<ROLE> roleList = new List<ROLE>();
            foreach (var item in userRoleList)
            {
                ROLE role = roleManage.GetById(item.ROLE_ID.Value);
                roleList.Add(role);
                perList.AddRange(rolefuMan.getFunctionByRoleId(role.ID));
            }
            Session["CurentUser"] = CurentUser;
            Session["UserContext"] = new UserContext
            {
                CurentUser = CurentUser,
                Permision = perList,
                ListRole = roleList,
                MainRoleCode = "",
                MainRoleName = "",
                ScreenView = "",
                IsStudent = CurentUser.TYPE == 1
            };

        }

    }
}