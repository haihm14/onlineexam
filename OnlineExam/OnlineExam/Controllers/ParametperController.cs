﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class ParametperController : Controller
    {
        // GET: parametper
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var parametper = JsonConvert.DeserializeObject<PARAMETPER>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            //parametper.CREATED_DATE = dt;
            //parametper.CREATED_BY = parametperContext.parametperName;
            parametper.STATUS = 1;
            int res = new ParametperManage().Insert(parametper);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var parametper = JsonConvert.DeserializeObject<PARAMETPER>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //parametper parametper = (parametper)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            //parametper.UPDATED_DATE = dt;
            //parametper.UPDATED_BY = parametperContext.parametperName;
            int res = new ParametperManage().Update(parametper);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new ParametperManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new ParametperManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new ParametperManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}