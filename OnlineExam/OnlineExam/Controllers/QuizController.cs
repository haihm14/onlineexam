﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class QuizController : Controller
    {
        // GET: quiz
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var quiz = JsonConvert.DeserializeObject<QUIZ>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            quiz.CREATED_DATE = dt;
            //quiz.CREATED_BY = quizContext.quizName;
            quiz.STATUS = 1;
            int res = new QuizManage().Insert(quiz);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var quiz = JsonConvert.DeserializeObject<QUIZ>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //quiz quiz = (quiz)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            quiz.UPDATED_DATE = dt;
            //quiz.UPDATED_BY = QuizContext.quizName;
            int res = new QuizManage().Update(quiz);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new QuizManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new QuizManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new QuizManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
        public string Search(string jsonstring)
        {
            var respon = new SimpleReponse();
            var quiz = JsonConvert.DeserializeObject<QUIZ>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            var list = new QuizManage().GetAll().Where(x=> x.STATUS == quiz.STATUS && x.PARENT_ID == quiz.PARENT_ID);
            respon.mes = list != null ? "OK" : "NOTOK";
            respon.data = JsonConvert.SerializeObject(list);
            return JsonConvert.SerializeObject(respon);

        }

        public string addQuestion(int subjectID, string jsonstring)
        {
            var respon = new SimpleReponse();

            List<EXAM_QUIZ> listID = new List<EXAM_QUIZ>();
            var lis = jsonstring.Split(',');
            foreach(var item in lis)
            {
                listID.Add(new EXAM_QUIZ {EXAM_ID= subjectID,QUIZ_ID= Int32.Parse(item) });
            }
            new ExamQuizManage().InsertBatch(listID);
            return JsonConvert.SerializeObject(respon);
        }
    }
}