﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class RoleController : Controller
    {
        // GET: role
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var role = JsonConvert.DeserializeObject<ROLE>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            role.CREATED_DATE = dt;
            //role.CREATED_BY = roleContext.roleName;
            role.STATUS = 1;
            int res = new RoleManage().Insert(role);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var role = JsonConvert.DeserializeObject<ROLE>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //role role = (role)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            //role.UPDATED_DATE = dt;
            //role.UPDATED_BY = roleContext.roleName;
            int res = new RoleManage().Update(role);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new RoleManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new RoleManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new RoleManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}