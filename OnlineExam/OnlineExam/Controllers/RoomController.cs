﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class RoomController : Controller
    {
        // GET: room
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var room = JsonConvert.DeserializeObject<ROOM>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            //room.CREATED_DATE = dt;
            //room.CREATED_BY = roomContext.roomName;
            room.STATUS = 1;
            int res = new RoomManage().Insert(room);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var room = JsonConvert.DeserializeObject<ROOM>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //room room = (room)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            //room.UPDATED_DATE = dt;
            ////room.UPDATED_BY = roomContext.roomName;
            int res = new RoomManage().Update(room);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new RoomManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new RoomManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new RoomManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}