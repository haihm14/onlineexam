﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class ScheduleController : Controller
    {
        // GET: schedule
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var schedule = JsonConvert.DeserializeObject<SCHEDULE>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            schedule.CREATED_DATE = dt;
            //schedule.CREATED_BY = scheduleContext.scheduleName;
            schedule.STATUS = 1;
            int res = new ScheduleManage().Insert(schedule);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var schedule = JsonConvert.DeserializeObject<SCHEDULE>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //schedule schedule = (schedule)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            //schedule.UPDATED_DATE = dt;
            //schedule.UPDATED_BY = scheduleContext.scheduleName;
            int res = new ScheduleManage().Update(schedule);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new ScheduleManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new ScheduleManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new ScheduleManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}