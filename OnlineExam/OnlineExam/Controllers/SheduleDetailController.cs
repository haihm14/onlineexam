﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class ScheduleDetailController : Controller
    {
        // GET: ScheduleDetail
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var ListScheduleDetail = JsonConvert.DeserializeObject<List<SCHEDULE_DETAIL>>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            int res = new ScheduleDetailManage().InsertBatch(ListScheduleDetail);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            var ScheduleDetail = JsonConvert.DeserializeObject<SCHEDULE_DETAIL>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            int res = new ScheduleDetailManage().Update(ScheduleDetail);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new ScheduleDetailManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new ScheduleDetailManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new ScheduleDetailManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}