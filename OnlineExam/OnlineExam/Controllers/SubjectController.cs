﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class SubjectController : Controller
    {
        // GET: subject
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var subject = JsonConvert.DeserializeObject<SUBJECT>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            subject.CREATED_DATE = dt;
           
            subject.STATUS = 1;
            int res = new SubjectManage().Insert(subject);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var subject = JsonConvert.DeserializeObject<SUBJECT>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //subject subject = (subject)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
           
            int res = new SubjectManage().Update(subject);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new SubjectManage().GetAll());
        }

        public string Detail(int id)
        {
            return JsonConvert.SerializeObject(new SubjectManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new SubjectManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }
    }
}