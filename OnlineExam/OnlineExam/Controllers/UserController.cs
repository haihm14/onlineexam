﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Manage;
using Models.EF;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Models.Manages;
using OnlineExam.Models;
using Newtonsoft.Json;

namespace OnlineExam.Controllers
{
    public class UserController : BaseController
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }


        public string Insert(string jsonstring)
        {
            var respon = new SimpleReponse();
            var user = JsonConvert.DeserializeObject<USER>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            DateTime dt = DateTime.Now;
            var curentUser = (USER)Session["CurentUser"];
            user.CREATED_DATE = dt;
            user.CREATED_BY = curentUser.USERNAME;
            user.STATUS = 1;
            int res = new UserManage().Insert(user);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }
        public string Update(string jsonstring)
        {
            var respon = new SimpleReponse();
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var user = JsonConvert.DeserializeObject<USER>(jsonstring, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //USER user = (USER)json_serializer.DeserializeObject(jsonstring);
            DateTime dt = DateTime.Now;
            var curentUser = (USER)Session["CurentUser"];
            user.CREATED_DATE = dt;
            user.CREATED_BY = curentUser.USERNAME;
            int res = new UserManage().Update(user);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
            //return RedirectToAction("Index","Home");
        }

        public string list()
        {
            return JsonConvert.SerializeObject(new UserManage().GetAll());
        }

        public string Detail(int id)
        {
            var a = new UserManage().GetById(id);
            return JsonConvert.SerializeObject(new UserManage().GetById(id));
        }

        public string Delete(int id)
        {
            var respon = new SimpleReponse();
            int res = new UserManage().Delete(id);
            respon.mes = res > 0 ? "OK" : "NOTOK";
            return JsonConvert.SerializeObject(respon);
        }

        public string GetExamByStudentID(int id)
        {
            // Call Schedule_detail =>  // check time currentTime >= timeSDM;
            int ExamID = new ScheduleDetailManage().GetAll().Where(x => x.USER_ID == id).First().EXAM_ID.Value;

            // id_exam ==> table_>Exam_Quiz --> GetQuiz
            List<EXAM_QUIZ> listEQ = new ExamQuizManage().GetAll().Where(x => x.EXAM_ID == ExamID).ToList<EXAM_QUIZ>();

            // list quiz
            List<QUIZ> listQ = new List<QUIZ>();

            var listQuiz = new QuizManage().GetAll().Where(x => x.STATUS == 1).ToList<QUIZ>();
            foreach (var item in listQuiz)
            {
                foreach (var item2 in listEQ)
                {
                    if (item.ID == item2.EXAM_ID || item.PARENT_ID == item2.EXAM_ID)
                        listQ.Add(item);
                }
            }
            // note check tim
            // return danh sach cau hoi
            var respon = new SimpleReponse();
            respon.data = JsonConvert.SerializeObject(listQ);
            respon.mes = "OK";

            return JsonConvert.SerializeObject(respon);
        }
    }
}