﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineExam.Models
{
    public class ExamModel
    {
        public int ID { get; set; }

        public string CODE { get; set; }

        public string NAME { get; set; }

        public int? TOTAL_TIME { get; set; }

        public int? SCHEDULE_ID { get; set; }

        public int? SUBJECT_ID { get; set; }

        public short? STATUS { get; set; }

        public string SHEDULE_NAME { get; set; }

        public string SUBJECT_NAME { get; set; }
    }
}