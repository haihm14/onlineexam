﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineExam.Models
{
    public class MenuModel
    {
        public int ID { get; set; }

        [StringLength(50)]
        public int PARENT_ID { get; set; }

        [StringLength(50)]
        public string NAME { get; set; }

        [StringLength(50)]
        public string URL { get; set; }

        [StringLength(50)]
        public string CREATED_BY { get; set; }

        public short? STATUS { get; set; }
    }
}
