﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineExam.Models
{
    public class SimpleReponse
    {
        public string httpCod { get; set; }
        public string mes { get; set; }
        public string data { get; set; }
    }
}