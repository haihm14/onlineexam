﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Models
{
    public class UserContext
    {
        public USER CurentUser { get; set; }
        public List<FUNCTION> Permision { get; set; }
        public string MainRoleCode { get; set; }
        public string MainRoleName { get; set; }
        public List<ROLE> ListRole { get; set; }
        public bool IsStudent { get; set; }
        public bool HasPermistion(string url)
        {
            if (Permision != null)
            {
                foreach (var funItem in Permision)
                {
                    if (funItem.URL.ToLower() == url.ToLower()) return true;
                }
            }

            return false;
        }
        public string ScreenView { get; set; }

    }
}
