﻿
var exam = lib("exam");
exam.initView();
function showDetail(self) {
    var id = $(self).parent().attr("rowId");
    exam.showDetailModal(id);
}
function showEdit(self) {
    var id = $(self).parent().attr("rowId");
    exam.showEditModal(id);
    $("#id-action").val("edit");
    $("#modal-titel-edit").html("Chỉnh sửa");
}
function saveAndClose() {
    exam.save($("#id-action").val());
}
function showAdd() {
    debugger;
    exam.showAddModal();
    $("#id-action").val("add");
    $("#modal-titel-edit").html("Thêm mới");
}
function search() {
    exam.search();
}
function remove() {
    var id = $(self).parent().attr("rowId");
    exam.remove(id);
}
function showAddQuest(self) {
    var idExam = $(self).parent().attr("rowId");
    $.ajax({
        type: "POST",
        url: "/Quiz/Search",
        cache: false,
        dataType: "json",
        data: { jsonstring: JSON.stringify({ STATUS: 1, PARENT_ID: 0 }) },
        async: false,
        beforeSend: function () {
        },
        success: function (data) {
            if (data.mes == "OK") {
                var list = JSON.parse(data.data);
                $("#list-quest-select").html('');
                for (var item in list) {
                    $("#list-quest-select").append("<option value='" + list[item].ID + "' >" + list[item].CONTENT + "</option>");
                }
            }
        },
        complete: function (data) {
        }
    });
    $("#subjectID").val(idExam);
    $("#question-modal").modal('show');
}
function addQuestion() {
    var listQuestion = $("#list-quest-select").val().join();
    var subject = $("#subjectID").val();
    $.ajax({
        type: "POST",
        url: "/Quiz/addQuestion",
        cache: false,
        dataType: "json",
        data: { jsonstring: listQuestion, subjectID: subject },
        async: false,
        beforeSend: function () {
        },
        success: function (data) {
            if (data.mes == "OK") {
            }
        },
        complete: function (data) {
        }
    });
}