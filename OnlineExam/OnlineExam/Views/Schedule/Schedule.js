﻿
var schedule = lib("schedule");
schedule.initView();
function showDetail(self) {
    var id = $(self).parent().attr("rowId");
    schedule.showDetailModal(id);
}
function showEdit(self) {
    var id = $(self).parent().attr("rowId");
    schedule.showEditModal(id);
    $("#id-action").val("edit");
    $("#modal-titel-edit").html("Chỉnh sửa");
}
function saveAndClose()
{
    schedule.save($("#id-action").val());
}
function showAdd() {
    debugger;
    schedule.showAddModal();
    $("#id-action").val("add");
    $("#modal-titel-edit").html("Thêm mới");
}
function search() {
    schedule.search();
}
function deleteInrow(self) {
    var id = $(self).parent().attr("rowId");
    schedule.remove(id);
}