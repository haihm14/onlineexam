﻿
var user = lib("user");
user.initView();
function showDetail(self) {
    var id = $(self).parent().attr("rowId");
    user.showDetailModal(id);
}
function showEdit(self) {
    var id = $(self).parent().attr("rowId");
    user.showEditModal(id);
    $("#id-action").val("edit");
    $("#modal-titel-edit").html("Chỉnh sửa");
}
function saveAndClose()
{
    user.save($("#id-action").val());
}
function showAdd() {
    debugger;
    user.showAddModal();
    $("#id-action").val("add");
    $("#modal-titel-edit").html("Thêm mới");
}
function search() {
    user.search();
}
function deleteInrow(self) {
    var id = $(self).parent().attr("rowId");
    user.remove(id);
}